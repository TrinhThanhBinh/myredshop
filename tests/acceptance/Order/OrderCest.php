<?php

use Step\AbstractStep;
use Step\OrderStep;
class OrderCest
{
    /**
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * @var string
     */
    protected $userNameAdmin, $passwordAdmin;

    /**
     * @var
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $product;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $zipCode;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $price;

    /**
     * @var string
     */
    protected $comment;

    /**
     * @var string
     */
    protected $requisitionNumber;

    /**
     * OrderCest constructor.
     */
    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
        $this->userNameAdmin ="admin";
        $this->passwordAdmin ="admin";
        $this->firstName = $this->faker->firstName;
        $this->lastName = $this->faker->lastName;
        $this->address = $this->faker->address;
        $this->zipCode = $this->faker->countryCode;
        $this->city = $this->faker->city;
        $this->phone = $this->faker->phoneNumber;
        $this->email = $this->faker->email;
        $this->username = $this->faker->userName;
        $this->password = "12345";
        $this->price = $this->faker->numberBetween(10,1000);
        $this->comment = $this->faker->text;
        $this->requisitionNumber = $this->faker->numberBetween(0,100);
        $this->product = "BinhShoes";
    }

    /**
     * @param OrderStep $I
     * @param AbstractStep $tester
     * @throws Exception
     */
    public function order(OrderStep $I, AbstractStep $tester){
        $tester->login($this->userNameAdmin, $this->passwordAdmin);
        $I->create_User($this->firstName, $this->lastName, $this->address, $this->zipCode, $this->city, $this->phone,
            $this->email, $this->username, $this->password);
        $I->createOrder($this->price, $this->comment, $this->requisitionNumber, $this->product);
        $I->check_Information($this->firstName, $this->lastName, $this->zipCode, $this->city, $this->product, $this->comment);
        //$I->updateOrderStatus($this->email);
        $I->delete_Order($this->email);
    }
}