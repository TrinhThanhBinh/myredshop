<?php

namespace Step;
use Page\CategoryPage;
use Page\AbstractPage;
class CategoryStep extends \AcceptanceTester
{
    /**
     * @param $nameCategory
     * @param $number
     * @param $description
     * @throws \Exception
     */
    /**
     * @param $nameCategory
     * @param $number
     * @param $description
     * @throws \Exception
     */
    public function create_Category($nameCategory, $number, $description){
        $I = $this;
        $I->amOnPage(CategoryPage::$urlCategoryPage);
        $I->waitForElementVisible(CategoryPage::$newButton, 30);
        $I->click(CategoryPage::$newButton);
        $I->waitForElementVisible(CategoryPage::$categoryName,30);
        $I->fillField(CategoryPage::$categoryName, $nameCategory);
        $I->attachFile(CategoryPage::$categoryImage,'sopet.jpg');
        $I->scrollTo(CategoryPage::$categoryName);
//        $I->waitForElementVisible(CategoryPage::$templateCategoryParents,30);
//        $I->click(CategoryPage::$templateCategoryParents);
        $I->waitForElementVisible(CategoryPage::$publicRadioButton,30);
        $I->click(CategoryPage::$publicRadioButton);
        $I->waitForElementVisible(CategoryPage::$numOfProduct,30);
        $I->fillField(CategoryPage::$numOfProduct, $number);
        $I->waitForElementVisible(CategoryPage::$templateDropdown,30);
        $I->click(CategoryPage::$templateDropdown);
        $I->waitForElementVisible(CategoryPage::$searchTemplateField,30);
        $I->fillField(CategoryPage::$searchTemplate, 'list');
        $I->waitForElementVisible(CategoryPage::$listTemplate,30);
        $I->click(CategoryPage::$listTemplate);
        $I->waitForElementVisible(CategoryPage::$allowTemplate,30);
        $I->fillField(CategoryPage::$allowTemplate, 'list');
        $I->waitForElementVisible(CategoryPage::$listSelect,30);
        $I->click(CategoryPage::$listSelect);
        $I->waitForElementVisible(CategoryPage::$productComparisionTemplate,30);
        $I->click(CategoryPage::$productComparisionTemplate);
        $I->waitForElementVisible(CategoryPage::$compareProductTemplate,30);
        $I->click(CategoryPage::$compareProductTemplate);
        $I->waitForElementVisible(CategoryPage::$editTool,30);
        $I->scrollTo(CategoryPage::$editTool);
        $I->waitForElementVisible(CategoryPage::$toggleEditorButton);
        $I->click(CategoryPage::$toggleEditorButton);
        $I->executeJS(CategoryPage::$jsWindow);
        $I->scrollTo(CategoryPage::$categoryName);
        $I->waitForElementVisible(CategoryPage::$editArea,30);
        $I->fillField(CategoryPage::$editArea, $description);
        $I->click(CategoryPage::$saveAndCloseButton);
        $I->waitForText(CategoryPage::$createSuccessMessage);
    }

    /**
     * @param $nameCategory
     * @param $number
     * @param $description
     * @throws \Exception
     */
    public function create_Category_Without_Fill_CategoryName($number, $description){
        $I = $this;
        $I->amOnPage(CategoryPage::$urlCategoryPage);
        $I->waitForElementVisible(CategoryPage::$newButton, 30);
        $I->click(CategoryPage::$newButton);
        $I->waitForElementVisible(CategoryPage::$categoryName,30);
        $I->attachFile(CategoryPage::$categoryImage,'sopet.jpg');
        $I->scrollTo(CategoryPage::$categoryName);
//        $I->waitForElementVisible(CategoryPage::$templateCategoryParents,30);
//        $I->click(CategoryPage::$templateCategoryParents);
        $I->waitForElementVisible(CategoryPage::$publicRadioButton,30);
        $I->click(CategoryPage::$publicRadioButton);
        $I->waitForElementVisible(CategoryPage::$numOfProduct,30);
        $I->fillField(CategoryPage::$numOfProduct, $number);
        $I->waitForElementVisible(CategoryPage::$templateDropdown,30);
        $I->click(CategoryPage::$templateDropdown);
        $I->waitForElementVisible(CategoryPage::$searchTemplateField,30);
        $I->fillField(CategoryPage::$searchTemplate, 'list');
        $I->waitForElementVisible(CategoryPage::$listTemplate,30);
        $I->click(CategoryPage::$listTemplate);
        $I->waitForElementVisible(CategoryPage::$allowTemplate,30);
        $I->fillField(CategoryPage::$allowTemplate, 'list');
        $I->waitForElementVisible(CategoryPage::$listSelect,30);
        $I->click(CategoryPage::$listSelect);
        $I->waitForElementVisible(CategoryPage::$productComparisionTemplate,30);
        $I->click(CategoryPage::$productComparisionTemplate);
        $I->waitForElementVisible(CategoryPage::$compareProductTemplate,30);
        $I->click(CategoryPage::$compareProductTemplate);
        $I->waitForElementVisible(CategoryPage::$editTool,30);
        $I->scrollTo(CategoryPage::$editTool);
        $I->waitForElementVisible(CategoryPage::$toggleEditorButton);
        $I->click(CategoryPage::$toggleEditorButton);
        $I->executeJS(CategoryPage::$jsWindow);
        $I->scrollTo(CategoryPage::$categoryName);
        $I->waitForElementVisible(CategoryPage::$editArea,30);
        $I->fillField(CategoryPage::$editArea, $description);
        $I->waitForElementVisible(CategoryPage::$btnSave);
        $I->click(CategoryPage::$btnSave);
        $I->executeJS(CategoryPage::$jsWindow);
        $I->waitForText(CategoryPage::$invalidFieldMessage,30);
    }

    /**
     * @param $categoryName
     * @throws \Exception
     */
    public function delete_Category($categoryName){
        $I = $this;
        $I->amOnPage(CategoryPage::$urlCategoryPage);
        $I->waitForElementVisible(CategoryPage::$searchField, 10);
        $I->fillField(CategoryPage::$searchField, $categoryName);
        $I->waitForElementVisible(CategoryPage::$btnSearch,30);
        $I->click(CategoryPage::$btnSearch);
        $I->waitForElementVisible(CategoryPage::$check,30);
        $I->checkOption(CategoryPage::$check);
        $I->waitForElementVisible(CategoryPage::$btnDelete,30);
        $I->click(CategoryPage::$btnDelete);
        $I->acceptPopup();
        $I->waitForText(CategoryPage::$deleteSuccessMessage,30);
    }

}