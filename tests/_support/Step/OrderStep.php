<?php

namespace Step;
use Page\OrderPage;

class OrderStep extends \AcceptanceTester
{
    /**
     * @param $firstName
     * @param $lastName
     * @param $address
     * @param $zipCode
     * @param $city
     * @param $phone
     * @param $email
     * @param $username
     * @param $password
     * @throws \Exception
     */
    public function create_User($firstName, $lastName, $address, $zipCode, $city, $phone, $email, $username, $password){
        $I = $this;
        $I->amOnPage(OrderPage::$urlOrderPage);
        $I->waitForElementVisible(OrderPage::$btnNew,30);
        $I->click(OrderPage::$btnNew);
        $I->waitForElementVisible(OrderPage::$firstName,30);
        $I->scrollTo(OrderPage::$firstName);
        $I->fillField(OrderPage::$firstName, $firstName);
        $I->waitForElementVisible(OrderPage::$lastName,30);
        $I->fillField(OrderPage::$lastName, $lastName);
        $I->waitForElementVisible(OrderPage::$address,30);
        $I->fillField(OrderPage::$address, $address);
        $I->waitForElementVisible(OrderPage::$zipCode,30);
        $I->fillField(OrderPage::$zipCode, $zipCode);
        $I->waitForElementVisible(OrderPage::$city,30);
        $I->fillField(OrderPage::$city, $city);
        $I->waitForElementVisible(OrderPage::$phone,30);
        $I->fillField(OrderPage::$phone, $phone);
        $I->waitForElementVisible(OrderPage::$email,30);
        $I->fillField(OrderPage::$email, $email);
        $I->waitForElementVisible(OrderPage::$userName, 30);
        $I->scrollTo(OrderPage::$userName);
        $I->fillField(OrderPage::$userName, $username);
        $I->waitForElementVisible(OrderPage::$newPassword,30);
        $I->fillField(OrderPage::$newPassword, $password);
        $I->waitForElementVisible(OrderPage::$confirmPassword,30);
        $I->fillField(OrderPage::$confirmPassword, $password);
        $I->waitForElementVisible(OrderPage::$btnApplyUser,30);
        $I->click(OrderPage::$btnApplyUser);
    }

    /**
     * @param $price
     * @param $comment
     * @param $requisitionNumber
     * @param $product
     * @throws \Exception
     */
    public function createOrder($price, $comment, $requisitionNumber, $product){
        $I = $this;
        $I->waitForElementVisible(OrderPage::$email,30);
        $I->scrollTo(OrderPage::$email);
        $I->wait(3);
        $I->waitForElementVisible(OrderPage::$productName,30);
        $I->click(OrderPage::$productName);
        $I->waitForElementVisible(OrderPage::$productSearchField,30);
        $I->fillField(OrderPage::$productSearchField, $product);
        $I->waitForElementVisible(OrderPage::$binhShoes,30);
        $I->click(OrderPage::$binhShoes);
        $I->fillField(OrderPage::$priceWithoutTax, $price);
        $I->waitForElementVisible(OrderPage::$comment,30);
        $I->scrollTo(OrderPage::$comment);
        $I->fillField(OrderPage::$comment, $comment);
        $I->waitForElementVisible(OrderPage::$requisitionNumber,30);
        $I->fillField(OrderPage::$requisitionNumber, $requisitionNumber);
        $I->waitForElementVisible(OrderPage::$selectStatus,30);
        $I->click(OrderPage::$selectStatus);
        $I->waitForElementVisible(OrderPage::$statusSearchField,30);
        $I->fillField(OrderPage::$statusSearchField, 'Pending');
        $I->waitForElementVisible(OrderPage::$pending);
        $I->click(OrderPage::$pending);
        $I->waitForElementVisible(OrderPage::$selectPaymentStatus,30);
        $I->click(OrderPage::$selectPaymentStatus);
        $I->waitForElementVisible(OrderPage::$paymentStatusSearchField,30);
        $I->fillField(OrderPage::$paymentStatusSearchField,'Unpaid');
        $I->waitForElementVisible(OrderPage::$unPaid, 30);
        $I->click(OrderPage::$unPaid);
        $I->waitForElementVisible(OrderPage::$btnSavePay,30);
        $I->click(OrderPage::$btnSavePay);
    }

    /**
     * @param $firstName
     * @param $lastName
     * @param $zipCode
     * @param $city
     * @param $product
     * @param $comment
     * @throws \Exception
     */
    public function check_Information($firstName, $lastName, $zipCode, $city, $product, $comment){
        $I = $this;
        $I->waitForElementVisible(OrderPage::$btnUpdateOrderStatus,30);
        $I->waitForElementVisible(OrderPage::$btnUpdate,30);
        $I->scrollTo(OrderPage::$btnUpdate);
        $I->waitForText($comment, 30);
        $I->waitForText($firstName,30);
        $I->waitForText($lastName, 30);
        $I->waitForElementVisible(OrderPage::$btnUpdateOrderStatus,30);
        $I->scrollTo(OrderPage::$btnUpdateOrderStatus);
        $I->waitForText($zipCode,30);
        $I->waitForText($city,30);
        $I->waitForElementVisible(OrderPage::$btnSave,30);
        $I->scrollTo(OrderPage::$btnSave);
        $I->waitForText($product,30);
        $I->waitForElementVisible(OrderPage::$statusLog,30);
        $I->scrollTo(OrderPage::$statusLog);
        $I->waitForText(OrderPage::$orderStatusMessage,30);
        $I->waitForElementVisible(OrderPage::$btnClose,30);
        $I->click(OrderPage::$btnClose);
    }

    /**
     * @param $email
     * @throws \Exception
     */
    public function updateOrderStatus($email){
        $I = $this;
        $I->amOnPage(OrderPage::$urlOrderPage);
        $I->waitForElementVisible(OrderPage::$searchField, 10);
        $I->fillField(OrderPage::$searchField, $email);
        $I->waitForElementVisible(OrderPage::$btnSearch,30);
        $I->click(OrderPage::$btnSearch);
        $I->scrollTo(OrderPage::$btnSearch);
        $I->wait(3);
        $I->waitForElementVisible(OrderPage::$btnUpdateOrder,30);
        $I->click(OrderPage::$btnUpdateOrder);
        $I->waitForElementVisible(OrderPage::$statusDropdown1,30);
        $I->click(OrderPage::$statusDropdown1);
        $I->waitForElementVisible(OrderPage::$searchStatus,30);
        $I->fillField(OrderPage::$searchStatus, 'Ready for 1st delivery');
        $I->waitForElementVisible(OrderPage::$readyFor1Deliver,30);
        $I->click(OrderPage::$readyFor1Deliver);
        $I->waitForElementVisible(OrderPage::$paymentStatusDropdown1,30);
        $I->click(OrderPage::$paymentStatusDropdown1);
        $I->waitForElementVisible(OrderPage::$searchPaymentStatus,30);
        $I->fillField(OrderPage::$searchPaymentStatus, 'Paid');
        $I->waitForElementVisible(OrderPage::$paid,30);
        $I->click(OrderPage::$paid);
        $I->waitForElementVisible(OrderPage::$saveCloseButton,30);
        $I->click(OrderPage::$saveCloseButton);
    }

    /**
     * @param $email
     * @throws \Exception
     */
    public function delete_Order($email){
        $I = $this;
        $I->amOnPage(OrderPage::$urlOrderPage);
        $I->waitForElementVisible(OrderPage::$searchField, 10);
        $I->fillField(OrderPage::$searchField, $email);
        $I->waitForElementVisible(OrderPage::$btnSearch,30);
        $I->click(OrderPage::$btnSearch);
        $I->wait(3);
        $I->waitForElementVisible(OrderPage::$check,30);
        $I->checkOption(OrderPage::$check);
        $I->waitForElementVisible(OrderPage::$btnDelete,30);
        $I->click(OrderPage::$btnDelete);
        $I->acceptPopup();
        $I->waitForText(OrderPage::$deleteSuccessMessage,30);
    }


}