<?php

namespace Step;
use Facebook\WebDriver\Remote\WebDriverBrowserType;
use Page\AbstractPage;
use Page\ProductPage;
class ProductStep extends \AcceptanceTester
{
    /**
     * @param $nameProduct
     * @param $numberProduct
     * @param $nameDescription
     * @throws \Exception
     */
    public function create_close_Product($nameProduct, $numberProduct, $nameDescription){
        $I = $this;
        $I->amOnPage(ProductPage::$urlProductPage);
        $I->waitForElementVisible(ProductPage::$btnNewProduct, 30);
        $I->click(ProductPage::$btnNewProduct);
        $I->waitForElementVisible(ProductPage::$txtNameProduct,30);
        $I->fillField(ProductPage::$txtNameProduct, $nameProduct);
        $I->waitForElementVisible(ProductPage::$txtNumberProduct,30);
        $I->fillField(ProductPage::$txtNumberProduct, $numberProduct);
        $I->waitForElementVisible(ProductPage::$txtProductCategory,30);
        $I->click(ProductPage::$txtProductCategory);
        $I->wait(3);
        $I->fillField(ProductPage::$txtProductCategory, 'Category rb');
        $I->wait(3);
        $I->pressKey(ProductPage::$txtProductCategory,\Facebook\WebDriver\WebDriverKeys::ENTER);
        $I->wait(3);
        //$I->attachFile(ProductPage::$imgProduct,'1.jpeg');
        //$I->wait(3);
        $I->attachFile(ProductPage::$imgProductBack,'sopet.jpg');
        $I->wait(3);
        //$I->fillField(ProductPage::$txtDescription, $nameDescription);
        $I->click(ProductPage::$btnSaveClose);
        $I->wait(3);
        //$I->waitForText(ProductPage::$createSuccessMessage);
    }

    /**
     * @param $nameProduct
     * @param $numberProduct
     * @throws \Exception
     */
    public function save_new_product($nameProduct, $numberProduct){
        $I = $this;
        $I->amOnPage(ProductPage::$urlProductPage);
        $I->waitForElementVisible(ProductPage::$btnNewProduct, 30);
        $I->click(ProductPage::$btnNewProduct);
        $I->waitForElementVisible(ProductPage::$txtNameProduct,30);
        $I->fillField(ProductPage::$txtNameProduct, $nameProduct);
        $I->waitForElementVisible(ProductPage::$txtNumberProduct,30);
        $I->fillField(ProductPage::$txtNumberProduct, $numberProduct);
        $I->waitForElementVisible(ProductPage::$txtProductCategory,30);
        $I->click(ProductPage::$txtProductCategory);
        $I->wait(3);
        $I->fillField(ProductPage::$txtProductCategory, 'Category rb');
        $I->wait(3);
        $I->pressKey(ProductPage::$txtProductCategory,\Facebook\WebDriver\WebDriverKeys::ENTER);
        $I->wait(3);
        //$I->attachFile(ProductPage::$imgProduct,'1.jpeg');
        //$I->wait(3);
        $I->attachFile(ProductPage::$imgProductBack,'sopet.jpg');
        $I->wait(3);
        $I->click(ProductPage::$btnSaveNew);
        $I->wait(3);
    }

    /**
     * @param $nameProduct
     * @param $numberProduct
     * @throws \Exception
     */
    public function edit_product($nameProduct, $numberProduct){
        $I = $this;
        $I->amOnPage(ProductPage::$urlProductPage);
        $I->waitForElementVisible(ProductPage::$chblistProduct, 30);
        $I->click(ProductPage::$chblistProduct);
        $I->waitForElementVisible(AbstractPage::$btnEdit, 30);
        $I->click(AbstractPage::$btnEdit);
        $I->waitForElementVisible(ProductPage::$txtNameProduct,30);
        $I->fillField(ProductPage::$txtNameProduct, $nameProduct);
        $I->waitForElementVisible(ProductPage::$txtNumberProduct,30);
        $I->fillField(ProductPage::$txtNumberProduct, $numberProduct);
        $I->waitForElementVisible(ProductPage::$txtProductCategory,30);
        $I->click(ProductPage::$txtProductCategory);
        $I->wait(3);
        $I->fillField(ProductPage::$txtProductCategory, 'Kimmy');
        $I->wait(3);
        $I->pressKey(ProductPage::$txtProductCategory,\Facebook\WebDriver\WebDriverKeys::ENTER);
        $I->wait(3);
        $I->attachFile(ProductPage::$imgProductBack,'sopet.jpg');
        $I->wait(3);
        $I->click(ProductPage::$btnSaveClose);
        $I->wait(3);
    }

    /**
     * @throws \Exception
     */
    public function delete_product(){
        $I = $this;
        $I->amOnPage(ProductPage::$urlProductPage);
        $I->wait(3);
        $I->waitForElementVisible(ProductPage::$chblistProduct, 30);
        $I->click(ProductPage::$chblistProduct);
        $I->wait(3);
        $I->waitForElementVisible(ProductPage::$btnDelete, 30);
        $I->click(ProductPage::$btnDelete);
        $I->acceptPopup();
        $I->wait(5);
        //$I->waitForText(AbstractPage::$deleteSuccessMessage);
    }

    /**
     * @param $nameProduct
     * @param $numberProduct
     * @throws \Exception
     */
    public function non_create($nameProduct, $numberProduct){
        $I = $this;
        $I->amOnPage(ProductPage::$urlProductPage);
        $I->waitForElementVisible(ProductPage::$btnNewProduct, 30);
        $I->click(ProductPage::$btnNewProduct);
        $I->waitForElementVisible(ProductPage::$txtNameProduct,30);
        $I->fillField(ProductPage::$txtNameProduct, $nameProduct);
        $I->waitForElementVisible(ProductPage::$txtNumberProduct,30);
        $I->fillField(ProductPage::$txtNumberProduct, $numberProduct);
        $I->attachFile(ProductPage::$imgProductBack,'sopet.jpg');
        $I->wait(3);
        $I->click(ProductPage::$btnSaveClose);
        $I->wait(5);
        $I->seeInPopup(ProductPage::$messCategoryFail);
        $I->wait(5);
        $I->acceptPopup();
        //$I->wait(5);
        //$I->waitForText(ProductPage::$messCategoryFail, 30);
    }
}
