<?php

namespace Page;


class AbstractPage
{
    /**
     * @var string
     */
    public static $ulrAdmin = "/administrator";

    /**
     * @var string
     */
    public static $userNameField = "#mod-login-username";

    /**
     * @var string
     */
    public static $passwordField = "#mod-login-password";

    /**
     * @var string
     */
    public static $loginButton = ".login-button";

    /**
     * @var string
     */
    public static $controlPanelTxt = "Control Panel";

    /**
     * @var string
     */
    public static $btnEdit = ".button-edit";

    /**
     * @var string
     */
    public static $checkBox = "#cb0";

    /**
     * @var string
     */
    public static $saveCloseButton = ".button-save";

    /**
     * @var string
     */
    public static $searchField = "#filter_search";

    /**
     * @var string
     */
    public static $createSuccessMessage = "Item saved.";

    /**
     * @var string
     */
    public static $editSuccessMessage = "Item edited.";

    /**
     * @var string
     */
    public static $deleteSuccessMessage = "Item deleted.";

}