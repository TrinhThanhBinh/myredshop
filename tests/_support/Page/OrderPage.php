<?php
/**
 * Created by PhpStorm.
 * User: n16dc
 * Date: 28/08/2020
 * Time: 9:37 CH
 */

namespace Page;


class OrderPage
{
    /**
     * @var string
     */
    public static $urlOrderPage = "administrator/index.php?option=com_redshop&view=order";

    /**
     * @var string
     */
    public static $btnNew = ".button-new";

    /**
     * @var string
     */
    public static $btnDelete = ".button-delete";

    /**
     * @var string
     */
    public static $searchField = "#filter";

    /**
     * @var string
     */
    public static $btnReset = ".btn reset";

    /**
     * @var string
     */
    public static $firstName = "#firstname";

    /**
     * @var string
     */
    public static $lastName = "#lastname";

    /**
     * @var string
     */
    public static $address = "#address";

    /**
     * @var string
     */
    public static $zipCode = "#zipcode";

    /**
     * @var string
     */
    public static $city = "#city";

    /**
     * @var string
     */
    public static $countryDropdown = "#s2id_rs_country_country_code";

    /**
     * @var string
     */
    public static $vietnam = "#select2-result-label-30e";

    /**
     * @var string
     */
    public static $phone = "#phone";

    /**
     * @var string
     */
    public static $email = "#email";

    /**
     * @var string
     */
    public static $userName = "#username";

    /**
     * @var string
     */
    public static $newPassword = "#password";

    /**
     * @var string
     */
    public static $confirmPassword = "#password2";

    /**
     * @var string
     */
    public static $btnApplyUser = ".button-apply";

    /**
     * @var string
     */
    public static $btnSavePay = "//div[@id='toolbar']//div[1]//button[1]";

    /**
     * @var string
     */
    public static $btnSaveWithoutMail = "//div[@id='toolbar']//div[2]//button[1]";

    /**
     * @var string
     */
    public static $btnSaveClose = "//div[@class='content-wrapper']//div//div[3]//button[1]";

    /**
     * @var string
     */
    public static $productName = "//div[@id='s2id_product1']//a[@class='select2-choice']";

    /**
     * @var string
     */
    public static $productSearchField = "#s2id_autogen2_search";

    /**
     * @var string
     */
    public static $binhShoes = "//span[contains(text(),'BinhShoes')]";

    /**
     * @var string
     */
    public static $priceWithoutTax = "#prdexclpriceproduct1";

    /**
     * @var string
     */
    public static $btnAddProduct = "//button[@class='btn btn-small button-apply btn-success']";

    /**
     * @var string
     */
    public static $comment = "//textarea[@name='customer_note']";

    /**
     * @var string
     */
    public static $requisitionNumber = "#requisition_number";

    /**
     * @var string
     */
    public static $selectStatus = "//div[@id='s2id_order_status']//a[@class='select2-choice']";

    /**
     * @var string
     */
    public static $statusSearchField = "#s2id_autogen8_search";

    /**
     * @var string
     */
    public static $pending = "//span[contains(text(),'Pending')]";

    /**
     * @var string
     */
    public static $selectPaymentStatus = "//div[@id='s2id_order_payment_status']//a[@class='select2-choice']";

    /**
     * @var string
     */
    public static $paymentStatusSearchField = "#s2id_autogen9_search";

    /**
     * @var string
     */
    public static $unPaid = "//span[@class='select2-match']";

    /**
     * @var string
     */
    public static $btnClose = ".button-cancel";

    /**
     * @var string
     */
    public static $messageOrder = "Order placed";

    /**
     * @var string
     */
    public static $jsWindow = 'window.scrollTo(0,0);';

    /**
     * @var string
     */
    public static $btnUpdateOrderStatus = "//input[@name='order_status']";

    /**
     * @var string
     */
    public static $btnUpdate = "//form[@id='updateshippingrate']//input[@id='add']";

    /**
     * @var string
     */
    public static $btnSave = ".btn-success";

    /**
     * @var string
     */
    public static $statusLog = "//h3[contains(text(),'Order Status Log')]";

    /**
     * @var string
     */
    public static $orderStatusMessage = "Order placed";

    /**
     * @var string
     */
    public static $check = "//input[@name='checkall-toggle']";

    /**
     * @var string
     */
    public static $btnSearch = "//button[@class='btn']";

    /**
     * @var string
     */
    public static $deleteSuccessMessage = "Order detail deleted successfully";

    /**
     * @var string
     */
    public static $btnUpdateOrder = "//button[@class='btn btn-default']";

    /**
     * @var string
     */
    public static $statusDropdown1 = "//div[@id='s2id_order_status18']//a[@class='select2-choice']";

    /**
     * @var string
     */
    public static $paymentStatusDropdown1 = "//div[@id='s2id_order_paymentstatus14']//a[@class='select2-choice']";

    /**
     * @var string
     */
    public static $customerNote = "//textarea[@name='customer_note14']";

    /**
     * @var string
     */
    public static $saveCloseButton = "//button[@class='button btn btn-primary']";

    /**
     * @var string
     */
    public static $readyFor1Deliver = "//span[@class='select2-match']";

    /**
     * @var string
     */
    public static $paid = "//div[@id='select2-result-label-215']";

    /**
     * @var string
     */
    public static $searchStatus = "//input[@id='s2id_autogen4_search']";

    /**
     * @var string
     */
    public static $searchPaymentStatus = "//input[@id='s2id_autogen5_search']";
}