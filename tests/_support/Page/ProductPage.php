<?php

namespace Page;

class ProductPage
{
    public static $urlProductPage = "administrator/index.php?option=com_redshop&view=product";

    /**
     * @var string
     */
    public static $txtNameProduct = '//input[@id=\'product_name\']';

    /**
     * @var string
     */
    public static $txtProductCategory = "//input[@id='s2id_autogen4']";

    /**
     * @var string
     */
    public static $imgProductBack = "//div[@class='form-group']//input[@id='product_back_full_image']";

    /**
     * @var string
     */
    public static $imgProduct2 = "//input[@type='file'][1]";

    /**
     * @var string
     */
    public static $imgProduct = "//div[@class='dz-default dz-message']";

    /**
     * @var string
     */
    public static $imgProduct1 = "//span[contains(text(),'Drop files here to upload')]";

    /**
     * @var string
     */
    public  static $txtNumberProduct = '//input[@id=\'product_number\']';

    /**
     * @var string
     */
    public static  $btnNewProduct = '//button[@class=\'btn btn-small button-new btn-success\']';

    /**
     * @var string
     */
    public static $chblistProduct = "//input[@id='cb0']";

    /**
     * @var string
     */
    public static $btnEdit = "//button[@class='btn btn-small button-edit']";

    /**
     * @var string
     */
    public static $btnSaveClose = "//button[@class='btn btn-small button-save']";

    /**
     * @var string
     */
    public static $btnSaveNew = ".button-save-new";

    /**
     * @var string
     */
    public static $btnDelete = "//div[4]//button[1]";

    /**
     * @var string
     */
    public static $txtDescription = "//div[@id='mceu_93']";

    /**
     * @var string
     */
    public static $createSuccessMessage = "Item saved.";

    /**
     * @var string
     */
    public static $editSuccessMessage = "Item edited.";

    /**
     * @var string
     */
    public static $deleteSuccessMessage = "Item deleted.";

    /**
     * @var string
     */
    public static $messCategoryFail = "Category must be selected";

}
