<?php

namespace Page;

class CategoryPage
{
    public static $urlCategoryPage = "administrator/index.php?option=com_redshop&view=categories";
    /**
     * @var string
     */
    public static $categoryMenuLink = "//a[@class='active']";

    /**
     * @var string
     */
    public static $newButton = ".button-new";

    /**
     * @var string
     */
    public static $btnDelete = ".button-delete";

    /**
     * @var string
     */
    public static $btnSave = ".button-save";

    /**
     * @var string
     */
    public static $saveAndCloseButton = ".button-save";

    /**
     * @var string
     */
    public static $saveAndNewButton = ".button-save-new";

    /**
     * @var string
     */
    public static $categoryName = "#jform_name";

    /**
     * @var string
     */
    public static $categoryParentDropdown = "#s2id_jform_parent_id";

    /**
     * @var string
     */
    public static $publicRadioButton = "#jform_published0";

    /**
     * @var string
     */
    public static $unPublicRadioButton = "#jform_published1";

    /**
     * @var string
     */
    public static $numOfProduct = "#jform_products_per_page";

    /**
     * @var string
     */
    public static $templateDropdown = "//div[@id='s2id_jform_template']//a[@class='select2-choice']";

    /**
     * @var string
     */
    public static $searchTemplateField = "#s2id_autogen10_search";

    /**
     * @var string
     */
    public static $listTemplate = "//span[@class='select2-match']";

    /**
     * @var string
     */
    public static $templateCategoryParents = "//ul[contains(@id,\"select2-results-9\")]//li[3]";

    /**
     * @var string
     */
    public static $allowTemplate = "//input[@id='s2id_autogen11']";

    /**
     * @var string
     */
    public static $listSelect = "//span[@class='select2-match']";

    /**
     * @var string
     */
    public static $productComparisionTemplate = "//div[@id='s2id_jform_compare_template_id']//a";

    /**
     * @var string
     */
    public static $compareProductTemplate = "//div[contains(text(),'compare_product')]";

    /**
     * @var string
     */
    public static $backImage = "#category_back_full_image-dropzone";

    /**
     * @var string
     */
    public static $categoryImage = "//input[@type='file'][1]";

    /**
     * @var string
     */
    public static $createBy = "jform_created_by";

    /**
     * @var string
     */
    public static $jsWindow = 'window.scrollTo(0,0);';

    /**
     * @var string
     */
    public static $toggleEditorButton = "//div[8]//div[1]//div[1]//div[2]//div[1]//a[1]";

    /**
     * @var string
     */
    public static $editArea = "//textarea[@id='jform_short_description']";

    /**
     * @var string
     */
    public static $editTool = "//div[@id='mceu_79-body']";

    /**
     * @var string
     */
    public static $createSuccessMessage = "Item saved.";

    /**
     * @var string
     */
    public static $searchTemplate = "#s2id_autogen10_search";

    /**
     * @var string
     */
    public static $invalidFieldMessage = "Invalid field:  Category";

    /**
     * @var string
     */
    public static $missingNumberMessage = "Invalid field:  No. of Products per Page";

    /**
     * @var string
     */
    public static $deleteSuccessMessage = "1 item successfully deleted";

    /**
     * @var string
     */
    public static $searchField = "#filter_search";

    /**
     * @var string
     */
    public static $btnSearch = "//input[@class='btn']";

    /**
     * @var string
     */
    public static $check = "//input[@name='checkall-toggle']";
}